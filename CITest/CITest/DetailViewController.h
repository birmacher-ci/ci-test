//
//  DetailViewController.h
//  CITest
//
//  Created by Bitrise on 06/09/16.
//  Copyright © 2016 citest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

