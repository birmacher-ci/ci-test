//
//  main.m
//  CITest
//
//  Created by Bitrise on 06/09/16.
//  Copyright © 2016 citest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
