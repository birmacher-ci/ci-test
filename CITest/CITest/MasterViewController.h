//
//  MasterViewController.h
//  CITest
//
//  Created by Bitrise on 06/09/16.
//  Copyright © 2016 citest. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

